{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "n8n.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "n8n.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate n8n certificate
*/}}
{{- define "n8n.n8n-certificate" }}
{{- if (not (empty .Values.ingress.n8n.certificate)) }}
{{- printf .Values.ingress.n8n.certificate }}
{{- else }}
{{- printf "%s-release-letsencrypt" .Release.Name }}
{{- end }}
{{- end }}

{{/*
Calculate n8n hostname
*/}}
{{- define "n8n.n8n-hostname" }}
{{- if (and .Values.config.n8n.hostname (not (empty .Values.config.n8n.hostname))) }}
{{- printf .Values.config.n8n.hostname }}
{{- else }}
{{- if .Values.ingress.n8n.enabled }}
{{- printf .Values.ingress.n8n.hostname }}
{{- else }}
{{- printf "%s-release.%s.svc.cluster.local" .Release.Name .Release.Namespace }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate n8n base url
*/}}
{{- define "n8n.n8n-base-url" }}
{{- if (and .Values.config.n8n.baseUrl (not (empty .Values.config.n8n.baseUrl))) }}
{{- printf .Values.config.n8n.baseUrl }}
{{- else }}
{{- if .Values.ingress.n8n.enabled }}
{{- $hostname := ((empty (include "n8n.n8n-hostname" .)) | ternary .Values.ingress.n8n.hostname (include "n8n.n8n-hostname" .)) }}
{{- $protocol := (.Values.ingress.n8n.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "n8n.n8n-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}
