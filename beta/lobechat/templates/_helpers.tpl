{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "lobechat.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "lobechat.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate lobechat certificate
*/}}
{{- define "lobechat.lobechat-certificate" }}
{{- if (not (empty .Values.ingress.lobechat.certificate)) }}
{{- printf .Values.ingress.lobechat.certificate }}
{{- else }}
{{- printf "%s-release-letsencrypt" .Release.Name }}
{{- end }}
{{- end }}

{{/*
Calculate lobechat hostname
*/}}
{{- define "lobechat.lobechat-hostname" }}
{{- if (and .Values.config.lobechat.hostname (not (empty .Values.config.lobechat.hostname))) }}
{{- printf .Values.config.lobechat.hostname }}
{{- else }}
{{- if .Values.ingress.lobechat.enabled }}
{{- printf .Values.ingress.lobechat.hostname }}
{{- else }}
{{- printf "%s-release.%s.svc.cluster.local" .Release.Name .Release.Namespace }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate lobechat base url
*/}}
{{- define "lobechat.lobechat-base-url" }}
{{- if (and .Values.config.lobechat.baseUrl (not (empty .Values.config.lobechat.baseUrl))) }}
{{- printf .Values.config.lobechat.baseUrl }}
{{- else }}
{{- if .Values.ingress.lobechat.enabled }}
{{- $hostname := ((empty (include "lobechat.lobechat-hostname" .)) | ternary .Values.ingress.lobechat.hostname (include "lobechat.lobechat-hostname" .)) }}
{{- $protocol := (.Values.ingress.lobechat.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "lobechat.lobechat-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}
