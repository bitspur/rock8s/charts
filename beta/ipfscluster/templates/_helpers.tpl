{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "ipfs-cluster.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "ipfs-cluster.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate gateway certificate
*/}}
{{- define "gateway.gateway-certificate" }}
{{- if (not (empty .Values.ingress.gateway.certificate)) }}
{{- printf .Values.ingress.gateway.certificate }}
{{- else }}
{{- printf "%s-release-letsencrypt" .Release.Name }}
{{- end }}
{{- end }}

{{/*
Calculate gateway hostname
*/}}
{{- define "gateway.gateway-hostname" }}
{{- if (and .Values.config.ipfsCluster.hostname (not (empty .Values.config.ipfsCluster.hostname))) }}
{{- printf .Values.config.ipfsCluster.hostname }}
{{- else }}
{{- if .Values.ingress.gateway.enabled }}
{{- printf .Values.ingress.gateway.hostname }}
{{- else }}
{{- printf "%s-release.%s.svc.cluster.local" .Release.Name .Release.Namespace }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate gateway base url
*/}}
{{- define "gateway.gateway-base-url" }}
{{- if (and .Values.config.ipfsCluster.baseUrl (not (empty .Values.config.ipfsCluster.baseUrl))) }}
{{- printf .Values.config.ipfsCluster.baseUrl }}
{{- else }}
{{- if .Values.ingress.gateway.enabled }}
{{- $hostname := ((empty (include "gateway.gateway-hostname" .)) | ternary .Values.ingress.gateway.hostname (include "gateway.gateway-hostname" .)) }}
{{- $protocol := (.Values.ingress.gateway.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "gateway.gateway-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate clusterApi certificate
*/}}
{{- define "clusterApi.clusterApi-certificate" }}
{{- if (not (empty .Values.ingress.clusterApi.certificate)) }}
{{- printf .Values.ingress.clusterApi.certificate }}
{{- else }}
{{- printf "%s-release-letsencrypt" .Release.Name }}
{{- end }}
{{- end }}

{{/*
Calculate clusterApi hostname
*/}}
{{- define "clusterApi.clusterApi-hostname" }}
{{- if (and .Values.config.ipfsCluster.hostname (not (empty .Values.config.ipfsCluster.hostname))) }}
{{- printf .Values.config.ipfsCluster.hostname }}
{{- else }}
{{- if .Values.ingress.clusterApi.enabled }}
{{- printf .Values.ingress.clusterApi.hostname }}
{{- else }}
{{- printf "%s-release.%s.svc.cluster.local" .Release.Name .Release.Namespace }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate clusterApi base url
*/}}
{{- define "clusterApi.clusterApi-base-url" }}
{{- if (and .Values.config.ipfsCluster.baseUrl (not (empty .Values.config.ipfsCluster.baseUrl))) }}
{{- printf .Values.config.ipfsCluster.baseUrl }}
{{- else }}
{{- if .Values.ingress.clusterApi.enabled }}
{{- $hostname := ((empty (include "clusterApi.clusterApi-hostname" .)) | ternary .Values.ingress.clusterApi.hostname (include "clusterApi.clusterApi-hostname" .)) }}
{{- $protocol := (.Values.ingress.clusterApi.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "clusterApi.clusterApi-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}
