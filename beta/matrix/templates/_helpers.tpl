{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "matrix.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "matrix.fullname" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Calculate synapse certificate
*/}}
{{- define "matrix.synapse-certificate" -}}
{{- if .Values.ingress.synapse.tls.certificate -}}
{{- printf .Values.ingress.synapse.tls.certificate -}}
{{- else -}}
{{- printf "%s-gateway" (include "matrix.name" .) -}}
{{- end -}}
{{- end -}}

{{/*
Calculate synapse hostname
*/}}
{{- define "matrix.synapse-hostname" -}}
{{- if .Values.config.synapse.hostname -}}
{{- printf .Values.config.synapse.hostname -}}
{{- else -}}
{{- if .Values.ingress.synapse.enabled -}}
{{- printf .Values.ingress.synapse.hostname -}}
{{- else -}}
{{- printf "%s-release-gateway.%s.svc.cluster.local" .Release.Name .Release.Namespace -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Calculate synapse base url
*/}}
{{- define "matrix.synapse-base-url" -}}
{{- if .Values.config.synapse.baseUrl -}}
{{- printf .Values.config.synapse.baseUrl -}}
{{- else -}}
{{- if .Values.ingress.synapse.enabled -}}
{{- $hostname := ((not (include "matrix.synapse-hostname" .)) | ternary .Values.ingress.synapse.hostname (include "matrix.synapse-hostname" .)) -}}
{{- $protocol := (.Values.ingress.synapse.tls.enabled | ternary "https" "http") -}}
{{- printf "%s://%s" $protocol $hostname -}}
{{- else -}}
{{- printf "http://%s" (include "matrix.synapse-hostname" .) -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Calculate matrix server name
*/}}
{{- define "matrix.server-name" -}}
{{- $hostnameParts := splitList "." (include "matrix.synapse-hostname" .) -}}
{{- printf "%s" (join "." (slice $hostnameParts (sub (len $hostnameParts) 2) (len $hostnameParts))) -}}
{{- end -}}

{{/*
Calculate federation certificate
*/}}
{{- define "matrix.federation-certificate" -}}
{{- if .Values.ingress.federation.tls.certificate -}}
{{- printf .Values.ingress.federation.tls.certificate -}}
{{- else -}}
{{- printf "%s-gateway" (include "matrix.name" .) -}}
{{- end -}}
{{- end -}}

{{/*
Calculate federation hostname
*/}}
{{- define "matrix.federation-hostname" -}}
{{- if .Values.config.federation.hostname -}}
{{- printf .Values.config.federation.hostname -}}
{{- else -}}
{{- if .Values.ingress.federation.enabled -}}
{{- printf .Values.ingress.federation.hostname -}}
{{- else -}}
{{- printf "%s-release-gateway.%s.svc.cluster.local" .Release.Name .Release.Namespace -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Calculate federation base url
*/}}
{{- define "matrix.federation-base-url" -}}
{{- if .Values.config.federation.baseUrl -}}
{{- printf .Values.config.federation.baseUrl -}}
{{- else -}}
{{- if .Values.ingress.federation.enabled -}}
{{- $hostname := ((not (include "matrix.federation-hostname" .)) | ternary .Values.ingress.federation.hostname (include "matrix.federation-hostname" .)) -}}
{{- $protocol := (.Values.ingress.federation.tls.enabled | ternary "https" "http") -}}
{{- printf "%s://%s" $protocol $hostname -}}
{{- else -}}
{{- printf "http://%s" (include "matrix.federation-hostname" .) -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Calculate element certificate
*/}}
{{- define "matrix.element-certificate" -}}
{{- if .Values.ingress.element.tls.certificate -}}
{{- printf .Values.ingress.element.tls.certificate -}}
{{- else -}}
{{- printf "%s-gateway" (include "matrix.name" .) -}}
{{- end -}}
{{- end -}}

{{/*
Calculate element hostname
*/}}
{{- define "matrix.element-hostname" -}}
{{- if .Values.config.element.hostname -}}
{{- printf .Values.config.element.hostname -}}
{{- else -}}
{{- if .Values.ingress.element.enabled -}}
{{- printf .Values.ingress.element.hostname -}}
{{- else -}}
{{- printf "%s-release-gateway.%s.svc.cluster.local" .Release.Name .Release.Namespace -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Calculate element base url
*/}}
{{- define "matrix.element-base-url" -}}
{{- if .Values.config.element.baseUrl -}}
{{- printf .Values.config.element.baseUrl -}}
{{- else -}}
{{- if .Values.ingress.element.enabled -}}
{{- $hostname := ((not (include "matrix.element-hostname" .)) | ternary .Values.ingress.element.hostname (include "matrix.element-hostname" .)) -}}
{{- $protocol := (.Values.ingress.element.tls.enabled | ternary "https" "http") -}}
{{- printf "%s://%s" $protocol $hostname -}}
{{- else -}}
{{- printf "http://%s" (include "matrix.element-hostname" .) -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Calculate coturn certificate
*/}}
{{- define "matrix.coturn-certificate" -}}
{{- if .Values.ingress.coturn.certificate -}}
{{- printf .Values.ingress.coturn.certificate -}}
{{- else -}}
{{- printf "%s-gateway" (include "matrix.name" .) -}}
{{- end -}}
{{- end -}}

{{/*
Calculate coturn hostname
*/}}
{{- define "matrix.coturn-hostname" -}}
{{- if .Values.config.coturn.hostname -}}
{{- printf .Values.config.coturn.hostname -}}
{{- else -}}
{{- printf "%s-release-gateway.%s.svc.cluster.local" .Release.Name .Release.Namespace -}}
{{- end -}}
{{- end -}}
